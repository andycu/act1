<%-- 
    Document   : index
    Created on : 21-04-2022, 11:00:26
    Author     : Alumno
    Alumno     : Claudio Curriel
    Seccion    : 1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
        <center>
        <style>
        body {
              background: #eb01a8;
              background-image: url("https://img.freepik.com/vector-gratis/lindo-circulo-pastel-gato-fisuras-patron-repetitivo-fondo-pantalla-lindo-fondo-transparente_42349-866.jpg?w=826"), linear-gradient(#eb01a8, #d13431); /* W3C */
        }
        </style>
          <body style="background-color:hsla(0,7%,50%,0.5);">
              <table>
              <img src="https://i.pinimg.com/originals/7a/d5/1e/7ad51e0c5b557a3d9d46bf0e4dfff9d3.png">
              <h1 style="color:rgb(80,0,0);">Hola soy un Gatito Informatico</h1>
              <h3 style="color:rgb(20,0,0);"><b><i>Solo estoy aqui para ayudarte a entender que solo estoy realizando esta actividad contigo <br>
                                                      Por tanto no esperes mucho de mi, mas que decirte que tengas un gran dia hoy. </i></b></h3>

                <div id="current_date"></p>
                <h1><b><script>
                date = new Date();
                year = date.getFullYear();
                month = date.getMonth() + 1;
                day = date.getDate();
                document.getElementById("current_date").innerHTML = day + "/" + month + "/" + year; 
            </script></b></h1>
          </table>
       </body>
    </center>
</html>
